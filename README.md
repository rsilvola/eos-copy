# CERN CI EOS Copy
Docker image to be used with GitLab CI to copy generic files or binaries from EOS.

## Version

1.8
  - Use SMB3 protocol for DFS by default, but allow users to configure it through the `SMB_PROTOCOL` variable.

1.7
  - Support deployment to EOS personal/project/workspace folder

1.6
  - Moved docker image to the GitLab registry

1.5
  - Support for [CERN DFS websites](https://espace.cern.ch/webservices-help/websitemanagement/ConfiguringCentrallyHostedSites-IIS7/Pages/default.aspx)

## Contents
* *eos-copy*: Publish in a EOS folder the contents provided. Makes use of environment variables, *Kerberos* for authentication and *lxplus.cern.ch* as bridge to access to EOS.
  * `EOS_ACCOUNT_USERNAME`: NICE username of the user responsible of the EOS deployment. Must have RW access to the EOS folder
  * `EOS_ACCOUNT_PASSWORD`: User's password
  * `TO_DIR`: Local folder to deploy the contents copied from `EOS_PATH`
  * `EOS_PATH`: EOS path where to be rsynced with EOS folder.
  * `EOS_MGM_URL` (optional): The MGM URL of the EOS instance. **Default:** `root://eosuser.cern.ch`

## Download
```sh
docker pull gitlab-registry.cern.ch/rsilvola/eos-copy
```
