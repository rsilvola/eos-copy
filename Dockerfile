FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
MAINTAINER rsilvola@cern.ch

# Install Kerberos client, rsync and ssh
RUN /usr/bin/yum install -y krb5-workstation xrootd-client eos-fuse

# Script that will rsync the EOS folder to the output directory
COPY eos-copy.sh /sbin/eos-copy
RUN chmod 755 /sbin/eos-copy
