#!/bin/bash

# April 2016 Borja Aparicio
# Receives:
# Environment variables
#   EOS_ACCOUNT_USERNAME
#   EOS_ACCOUNT_PASSWORD
#   TO_PATH => default: public/
#   EOS_PATH
#   EOS_MGM_URL => default: root://eosuser.cern.ch
#
#
# Produces:
#  Copies the files found in $EOS_PATH in the EOS namespace to TO_PATH
# 
# Usage:
# eos-copy [EOS_PATH] [TO_PATH]

# Authenticate user via Kerberos
kinit="/usr/bin/kinit"
if [ ! -x $kinit ]
then
        echo "ERROR: $kinit not found"
        exit 1
fi

kdestroy="/usr/bin/kdestroy"
if [ ! -x $kdestroy ]
then
        echo "ERROR: $kdestroy not found"
        exit 1
fi

# XROOTD client to copy files to EOS
xrdcp="/usr/bin/xrdcp"
if [ ! -x $xrdcp ]
then
        echo "ERROR: $xrdcp not found"
        exit 1
fi

if [ $# -eq 2 ]
then
	EOS_PATH=$1
	TO_PATH=$2
fi

# Validate input
: "${EOS_ACCOUNT_USERNAME:?EOS_ACCOUNT_USERNAME not provided}"
: "${EOS_ACCOUNT_PASSWORD:?EOS_ACCOUNT_PASSWORD not provided}"
: "${EOS_PATH:?EOS_PATH not provided}"

# Check the source directory exists
if [ ! -d "$TO_PATH" ]
then
	echo "ERROR: Destination directory '$TO_PATH' doesn't exist"
	exit 1
fi

# EOS MGM URL, if not provided by the user
if [ "X$EOS_MGM_URL" == "X" ]
then
	EOS_MGM_URL="root://eosuser.cern.ch"
fi

# Get credentials
echo "$EOS_ACCOUNT_PASSWORD" | $kinit "$EOS_ACCOUNT_USERNAME@CERN.CH" 2>&1 >/dev/null
if [ $? -ne 0 ]
then
	echo "Failed to get Krb5 credentials for '$EOS_ACCOUNT_USERNAME'"
        exit 1
fi

# Rely in xrootd to do the copy of files to EOS. The destination path is created
# as necessary.
$xrdcp --force --recursive --silent --path "$EOS_MGM_URL/$EOS_PATH/" "$TO_PATH/"
if [ $? -ne 0 ]
then
    echo "ERROR: Failed to copy files from '$EOS_PATH' to '$TO_PATH/' via xrdcp"
    exit 1
fi

# Destroy credentials
$kdestroy
if [ $? -ne 0 ]
then
	echo "Krb5 credentials for '$DFS_ACCOUNT_USERNAME' have not been cleared up"
fi

echo "Successfully synced '$EOS_PATH' to '$TO_PATH'"
exit 0
